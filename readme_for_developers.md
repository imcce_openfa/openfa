Developers
==========

This section is dedicated to the developers of the library.

Update of the SOFA version
--------------------------

Execute the following commands

```shell
cd fetch && ./update_sofa.sh
```

Update the version in the file README.md and CMakeLists.txt.

Contents of the directory
-------------------------
- fetch : download and update the sofa version.
- src : directory generated by fetch/update_sofa.sh.
- tests : test programs of the library.
- extra : extra headers and sources of the library.


Creation of distribution packages
---------------------------------

To create a debian package with a prefix root path

```shell
cpack -G DEB -D CPACK_PACKAGING_INSTALL_PREFIX=/opt/openfa/
```

To create a rpm package with a prefix root path

```shell
cpack -G RPM -D CPACK_PACKAGING_INSTALL_PREFIX=/opt/openfa/
```