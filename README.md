# Open Fundamental Astronometry 20231011.0.3

OpenFA is a C library based on the SOFA library (http://www.iausofa.org/).
OpenFA is intended to replicate the functionality of SOFA, but is licensed under a three-clause BSD license to enable its compatibility with a wide range of open source licenses. 

This library OpenFA is maintained by IMCCE (https://www.imcce.fr/).

The licensing terms of the SOFA library, published by the SOFA Board appointed through the International Astronomical Union (IAU) Division A, are respected and detailed in the section *naming convention*.

Building and installing OpenFA
==============================

Requirements 
 - cmake 3.0.0 or later
 - C compiler


Some binary packages are vailable. The installation process is described on https://imcce_openfa.pages.obspm.fr/openfa/


To build from the sources and install a released version of OpenFA in your OS's standard location, simply do::

```shell     
    mkdir build
    cd build
    cmake -DCMAKE_INSTALL_PREFIX=/usr/local/  ../ 
    make
    make test
    make install
```

You could replace the path */usr/local/* by the location of your choice, e.g. */usr/local/openfa*
 
You can change the C compiler with the option *-DCMAKE_C_COMPILER=...* on the cmake command line.

Naming convention
=================

Version number convention
-------------------------


The version numbers  of OpenFA is based on three numbers : MAJOR.MINOR.PATCH, based on the SOFA version.
 
 - MAJOR is the date of the SOFA version (e.g., 20180130 -> 2018-01-30 of SOFA)
 - MINOR corresponds to the letter of the revision ( 0 -> initial revision of SOFA, 1 -> a, 2 -> b, ...)
 - PATCH is the patch number of the openfa revision. ( 0 -> initial revision, 1 -> a, 2 -> b, ...)


The version of OpenFA (20200721.0.0) is based on the SOFA version "2020-07-21", with the differences outlined below.

OpenFA prefixes
---------------

To respect the licensing terms of SOFA, all references to "SOFA" in the source code have been changed to OpenFA, and
functions have the prefix ``openfa`` instead of ``iau_``.

C macro prefixes
----------------

To respect the licensing terms of SOFA, all C macros used in OpenFA are the same as their SOFA equivalents, but with an
``OpenFA_`` prefix.

Additions
---------

The following macros and functions were added to be able to retrieve the version  of the library.

 - OPENFA_VERSION_MAJOR  /* major version of the library as an integer */
 - OPENFA_VERSION_MINOR  /* minor version of the library as an integer */
 - OPENFA_VERSION_PATCH  /* patch version of the library as an integer */
 - OPENFA_VERSION_STRING /* version of the library as a string "major.minor.patch" */
 - const char *openfa_getversion(void);  /* return the full version of the library as a string "major.minor.patch" */




