OpenFA is a C library based on the SOFA library (http://www.iausofa.org/) 
published by the International Astronomical Union (IAU). 
This library is maintained by IMCCE (https://www.imcce.fr/).
OpenFA is intended to replicate the functionality of SOFA, but is licensed
under a three-clause BSD license to enable its compatibility with a wide
range of open source licenses. 
