# ####################################################################
# create and install the openfaConfig.cmake and openfaConfigVersion.cmake
#
# Copyright, 2021-2022, IMCCE, CNRS, Observatoire de Paris, PSL.
#
# ####################################################################
# see https://gitlab.kitware.com/cmake/community/wikis/doc/tutorials/How-to-create-a-ProjectConfig.cmake-file
set(DEF_INSTALL_CMAKE_DIR "${CMAKE_INSTALL_LIBDIR}/cmake/openfa")

set(INSTALL_CMAKE_DIR ${DEF_INSTALL_CMAKE_DIR} CACHE PATH "Installation directory for CMake files")

# Make relative paths absolute
if(OPENFA_INSTALL)
  foreach(p LIB BIN INCLUDE SYSCONFIG) # CMAKE
    set(var INSTALL_${p}_DIR)

    if(NOT IS_ABSOLUTE "${${var}}")
      set(${var} "${CMAKE_INSTALL_PREFIX}/${${var}}")
    endif()
  endforeach()
endif()

# Create the openfaConfig.cmake and openfaConfigVersion.cmake
include(CMakePackageConfigHelpers)
configure_package_config_file(toolchain/openfaConfig.cmake.in ${PROJECT_BINARY_DIR}/openfaConfig.cmake
  INSTALL_DESTINATION "${INSTALL_CMAKE_DIR}" PATH_VARS INSTALL_INCLUDE_DIR INSTALL_SYSCONFIG_DIR)
write_basic_package_version_file(${PROJECT_BINARY_DIR}/openfaConfigVersion.cmake VERSION ${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}.${PROJECT_VERSION_PATCH} COMPATIBILITY AnyNewerVersion)

# Install the openfaConfig.cmake and openfaConfigVersion.cmake
if(OPENFA_INSTALL)
  install(FILES "${PROJECT_BINARY_DIR}/openfaConfig.cmake" "${PROJECT_BINARY_DIR}/openfaConfigVersion.cmake"
    DESTINATION "${INSTALL_CMAKE_DIR}" COMPONENT dev)

  # Install the export set for use with the install-tree
  install(EXPORT openfaTargets DESTINATION "${INSTALL_CMAKE_DIR}" COMPONENT dev)
endif()
