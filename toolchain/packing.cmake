# ####################################################################
# packaging for tgz, deb
#
# Copyright, 2021-2024, IMCCE, CNRS, Observatoire de Paris, PSL.
#
# ####################################################################
set(CPACK_SOURCE_GENERATOR "TGZ")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "Library replicates the algorithms of the library SOFA")
set(CPACK_PACKAGE_VENDOR "IMCCE")
set(CPACK_PACKAGE_DESCRIPTION_FILE ${CMAKE_CURRENT_SOURCE_DIR}/toolchain/readme_short.md)
set(CPACK_PACKAGE_HOMEPAGE_URL "https://gitlab.obspm.fr/imcce_openfa/openfa")
set(CPACK_SOURCE_PACKAGE_FILE_NAME
  "${CMAKE_PROJECT_NAME}-${PROJECT_VERSION_MAJOR}.${PROJECT_VERSION_MINOR}.${PROJECT_VERSION_PATCH}")
set(CPACK_SOURCE_IGNORE_FILES
  "/build/;/.git/;~$;${CPACK_SOURCE_IGNORE_FILES};/.vscode/;/.git")

set(CPACK_PACKAGE_INSTALL_DIRECTORY ${CPACK_PACKAGE_NAME})
set(CPACK_OUTPUT_FILE_PREFIX "${CMAKE_CURRENT_BINARY_DIR}/_packages")
set(CPACK_SET_DESTDIR false)

set(CPACK_PACKAGE_VERSION_MAJOR ${PROJECT_VERSION_MAJOR})
set(CPACK_PACKAGE_VERSION_MINOR ${PROJECT_VERSION_MINOR})
set(CPACK_PACKAGE_VERSION_PATCH ${PROJECT_VERSION_PATCH})

set(CPACK_PACKAGE_CONTACT ${OPENFA_CONTACT_EMAIL})

set(CPACK_RESOURCE_FILE_LICENSE "${CMAKE_CURRENT_SOURCE_DIR}/LICENSE")
set(CPACK_RESOURCE_FILE_README "${CMAKE_CURRENT_SOURCE_DIR}/README.md")

# ####################################################################
# Debian packaging
set(CPACK_DEBIAN_FILE_NAME DEB-DEFAULT)
set(CPACK_COMPONENTS_GROUPING ONE_PER_GROUP)
set(CPACK_DEB_COMPONENT_INSTALL YES)
set(CPACK_DEBIAN_PACKAGE_DEPENDS "")
set(CPACK_DEBIAN_PACKAGE_MAINTAINER "${OPENFA_MAINTAINER_NAME} <${CPACK_PACKAGE_CONTACT}>")

# ####################################################################
# RPM packaging
set(CPACK_RPM_PACKAGE_LICENSE "BSD")

# ####################################################################
# replace the following lines in future : should add changelog and copyright : see https://gitlab.kitware.com/cmake/cmake/-/issues/21832
install(FILES "copyright" DESTINATION "${CMAKE_INSTALL_DOCDIR}" COMPONENT dev)

include(CPack)
