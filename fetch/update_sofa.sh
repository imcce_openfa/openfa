#! /bin/bash

# Stop the script when an error occurs ("1" return)
set -e

# Macro to detect changes and eventually update files or directrories
# The global variable updateNeeded is set to "y" if changes have been detected but not updated
changesDetectionReplace ()
{
  # Checks the arguments
  case $# in
    1) local pathToUpdate=$1 ;;
    *) echo "This macro needs 1 argument." ; exit 1 ;;
  esac

  local sourcePath="./openfa/${pathToUpdate}"
  local destPath="../${pathToUpdate}"

  if ! diff -brq ${sourcePath} ${destPath} > diffOut.tmp ; then
    #echo "${sourcePath} and ${destPath} differ:"
    cat diffOut.tmp | sort
    if [ "${activeReplace}" = "n" ] ; then
      updateNeeded=y
    else
      rm -rf ${destPath} && mv ${sourcePath} ${destPath}
      echo "${destPath} updated!"
    fi
  else
    #echo "No change, no need to update"
    rm -rf ${sourcePath}
  fi

  # Remove temporary files
  rm -f diffOut.tmp
}

activeReplace=n
# Test if "-r" option is present
while getopts ":r" opt; do
  case $opt in
    r)  activeReplace=y ;; # Automatically replace the files if needed
    \?) echo "Invalid option: -$OPTARG" ; exit 1 ;;
  esac
done

# Remove previous download of sofa*.tar.gz
rm -rf sofa_c*.tar.gz openfa

# Download the latest version of SOFA
python3 download_sofa.py -q

# Re-create the tree in openfa
  mkdir -p openfa/src openfa/tests
  mv openfa/t_openfa_c.c openfa/tests/.
  mv openfa/*.* openfa/src/.

# Create the src/CMakeLists.txt
LISTFILES=$(cd ./openfa/src && ls *.c | sort)
cat - >./openfa/src/CMakeLists.txt <<__EOF
#####################################################################
#  build chain for the source of the library.
#
#  Copyright, 2018-2024 IMCCE, CNRS, Observatoire de Paris, PSL.
#
#####################################################################
add_library(openfa
            $LISTFILES
            ../extra/openfaextra.c
)
if (BUILD_SHARED_LIBS)
    set_target_properties(openfa PROPERTIES VERSION \${OPENFA_SOVERSION_FULL} SOVERSION \${OPENFA_SOVERSION_COMPAT})
    target_link_libraries(openfa \${LIBM_LIBRARIES})
endif()


#####################################################################
# installation step
#####################################################################
install(FILES openfa.h openfam.h COMPONENT dev DESTINATION "include/")
install(TARGETS  openfa EXPORT openfaTargets ARCHIVE COMPONENT dev  DESTINATION "\${CMAKE_INSTALL_LIBDIR}" LIBRARY COMPONENT lib  DESTINATION "\${CMAKE_INSTALL_LIBDIR}")
if (WIN32)
    if (BUILD_SHARED_LIBS)
        install(TARGETS openfa RUNTIME DESTINATION "\${CMAKE_INSTALL_BINDIR}")
    endif()
endif()
target_include_directories(openfa PUBLIC $<INSTALL_INTERFACE:include/> )
target_include_directories(openfa PUBLIC $<BUILD_INTERFACE:\${CMAKE_CURRENT_BINARY_DIR}/../extra>  )
target_include_directories(openfa PUBLIC $<BUILD_INTERFACE:>  )
target_include_directories(openfa INTERFACE $<INSTALL_INTERFACE:include/> )
target_include_directories(openfa INTERFACE $<BUILD_INTERFACE:\${CMAKE_CURRENT_BINARY_DIR}/../extra>  )
target_include_directories(openfa INTERFACE $<BUILD_INTERFACE:\${CMAKE_CURRENT_SOURCE_DIR}>  )

__EOF
sed  "/^add_library(openfa$/,/\.\.\/extra\/openfaextra.c/s:^[a-z].*\.c:\            &:g" ./openfa/src/CMakeLists.txt > ./openfa/src/CMakeLists.txt.tmp
mv ./openfa/src/CMakeLists.txt.tmp ./openfa/src/CMakeLists.txt


# Extract the version identification of sofa from sofa*.tar.gz
  fileName=$(ls sofa_c*.tar.gz)
  versionSofa=${fileName%%.tar.gz} ; versionSofa=${versionSofa#sofa_c-}
  versionSofaMajor=${versionSofa%%_*}
  versionSofaMinorLetter=${versionSofa#${versionSofaMajor}}
  versionSofaMinorLetter=${versionSofaMinorLetter#_}
  if [ -z "${versionSofaMinorLetter}" ] ; then
    # If no letter, set versionSofaMinor to zero
    versionSofaMinor=0
  else
    # Convert letters a,b,c,...,z to 1,2,3,...,26
    versionSofaMinor=$(( $(printf '%d' "'${versionSofaMinorLetter}")-96))
  fi
  # SOFA archive is now useless, remove it
  rm -f sofa_c*.tar.gz
  # Update openfa/CMakeLists.txt
  sed "/^\ *project\ *(/s:[0-9]\{8\}\.[0-9]\+:${versionSofaMajor}.${versionSofaMinor}:g" ../CMakeLists.txt >./openfa/CMakeLists.txt
  # If ../CMakeLists.txt  and  ./openfa/CMakeLists.txt are different, then set the PATCH version to zero
  if ! cmp -s ../CMakeLists.txt ./openfa/CMakeLists.txt ; then
    sed "/^\ *project\ *(/s:\([0-9]\{8\}\.[0-9]\+\)\.[0-9]*:\1.0:g" ./openfa/CMakeLists.txt > ./openfa/CMakeLists.txt.tmp
    mv ./openfa/CMakeLists.txt.tmp ./openfa/CMakeLists.txt
  fi

# Detection of changes and eventually update
updateNeeded=n
changesDetectionReplace src
changesDetectionReplace tests/t_openfa_c.c
changesDetectionReplace CMakeLists.txt

if [ "${updateNeeded}" = "y" ] && [ "${activeReplace}" = "n" ] ; then
  echo "Some files need to be updated; please re-run with '-r' option to replace:"
  echo "  $0 -r"
fi

# Remove all empty directories of openfa/*
find ./openfa -depth -type d -empty -exec rmdir {} \;

