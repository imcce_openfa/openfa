
/*
**  - - - - - - - - - - - - -
**   t _ v e r s i o n . c
**  - - - - - - - - - - - - -
**
**  tests for the extra functions for OpenFA library.
**
**  Copyright (C) 2018-2020,  IMCCE, CNRS, Observatoire de Paris, PSL.
*/

#include "openfaextra.h"
#include <stdio.h>

/* check the version number as string */
int main(int argc, char *argv[]) {
  const char *p1, *p2;
  printf("argc=%d argv=%p\n", argc, argv);
  printf("openfa_getversion='%s'\n", openfa_getversion());
  printf("MAJOR.MINOR.PATCH=%d.%d.%d\n", OPENFA_VERSION_MAJOR,
         OPENFA_VERSION_MINOR, OPENFA_VERSION_PATCH);
  printf("OPENFA_VERSION_STRING='%s'\n", OPENFA_VERSION_STRING);
  p1 = openfa_getversion();
  p2 = OPENFA_VERSION_STRING;
  while (*p1 != '\0' && *p2 != '\0' && *p1 == *p2) {
    p1++;
    p2++;
  }
  if (*p1 != '\0')
    return 1;
  if (*p2 != '\0')
    return 1;
  return 0;
}

/*----------------------------------------------------------------------
**
**
**  Copyright (C) 2018-2018,  IMCCE, CNRS, Observatoire de Paris, PSL.
**  All rights reserved.
**
**  This library is derived, with permission, from the International
**  Astronomical Union's "Standards of Fundamental Astronomy" library,
**  available from http://www.iausofa.org
**  and is derived from the ERFA project https://github.com/liberfa/erfa.
**
**  The OpenFA version is intended to retain identical functionality to
**  the SOFA library, but made distinct through different function and
**  file names, as set out in the SOFA license conditions.  The SOFA
**  original has a role as a reference standard for the IAU and IERS,
**  and consequently redistribution is permitted only in its unaltered
**  state.  The OpenFA version is not subject to this restriction and
**  therefore can be included in distributions which do not support the
**  concept of "read only" software.
**
**  Although the intent is to replicate the SOFA API (other than
**  replacement of prefix names) and results (with the exception of
**  bugs), SOFA is not
**  responsible for any errors found in this version of the library.
**
**  If you wish to acknowledge the SOFA heritage, please acknowledge
**  that you are using a library derived from SOFA, rather than SOFA
**  itself.
**
**
**  TERMS AND CONDITIONS
**
**  Redistribution and use in source and binary forms, with or without
**  modification, are permitted provided that the following conditions
**  are met:
**
**  1 Redistributions of source code must retain the above copyright
**    notice, this list of conditions and the following disclaimer.
**
**  2 Redistributions in binary form must reproduce the above copyright
**    notice, this list of conditions and the following disclaimer in
**    the documentation and/or other materials provided with the
**    distribution.
**
**  3 Neither the name of the Standards Of Fundamental Astronomy Board,
**    the International Astronomical Union nor the names of its
**    contributors may be used to endorse or promote products derived
**    from this software without specific prior written permission.
**
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
**  COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE.
**
*/