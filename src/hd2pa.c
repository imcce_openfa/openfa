#include "openfa.h"

double openfaHd2pa (double ha, double dec, double phi)
/*
**  - - - - - - - - -
**   o p e n f a H d 2 p a
**  - - - - - - - - -
**
**  Parallactic angle for a given hour angle and declination.
**
**  Given:
**     ha     double     hour angle
**     dec    double     declination
**     phi    double     site latitude
**
**  Returned (function value):
**            double     parallactic angle
**
**  Notes:
**
**  1)  All the arguments are angles in radians.
**
**  2)  The parallactic angle at a point in the sky is the position
**      angle of the vertical, i.e. the angle between the directions to
**      the north celestial pole and to the zenith respectively.
**
**  3)  The result is returned in the range -pi to +pi.
**
**  4)  At the pole itself a zero result is returned.
**
**  5)  The latitude phi is pi/2 minus the angle between the Earth's
**      rotation axis and the adopted zenith.  In many applications it
**      will be sufficient to use the published geodetic latitude of the
**      site.  In very precise (sub-arcsecond) applications, phi can be
**      corrected for polar motion.
**
**  6)  Should the user wish to work with respect to the astronomical
**      zenith rather than the geodetic zenith, phi will need to be
**      adjusted for deflection of the vertical (often tens of
**      arcseconds), and the zero point of the hour angle ha will also
**      be affected.
**
**  Reference:
**     Smart, W.M., "Spherical Astronomy", Cambridge University Press,
**     6th edition (Green, 1977), p49.
**
**  Last revision:   2017 September 12
**
**  OPENFA release 2023-10-11
**
**  Copyright (C) 2023 IAU OPENFA Board.  See notes at end.
*/
{
   double cp, cqsz, sqsz;


   cp = cos(phi);
   sqsz = cp*sin(ha);
   cqsz = sin(phi)*cos(dec) - cp*sin(dec)*cos(ha);
   return ( ( sqsz != 0.0 || cqsz != 0.0 ) ? atan2(sqsz,cqsz) : 0.0 );

/* Finished. */

}
/*----------------------------------------------------------------------
**  
**  
**  Copyright (C) 2018-2024,  IMCCE, CNRS, Observatoire de Paris, PSL.
**  All rights reserved.
**  
**  This library is derived, with permission, from the International
**  Astronomical Union's "Standards of Fundamental Astronomy" library,
**  available from http://www.iausofa.org
**  and is derived from the ERFA project https://github.com/liberfa/erfa.
**  
**  The OPENFA version is intended to retain identical functionality to
**  the SOFA library, but made distinct through different function and
**  file names, as set out in the SOFA license conditions.  The SOFA
**  original has a role as a reference standard for the IAU and IERS,
**  and consequently redistribution is permitted only in its unaltered
**  state.  The OPENFA version is not subject to this restriction and
**  therefore can be included in distributions which do not support the
**  concept of "read only" software.
**  
**  Although the intent is to replicate the SOFA API (other than
**  replacement of prefix names) and results (with the exception of
**  bugs), SOFA is not
**  responsible for any errors found in this version of the library.
**  
**  If you wish to acknowledge the SOFA heritage, please acknowledge
**  that you are using a library derived from SOFA, rather than SOFA
**  itself.
**  
**  
**  TERMS AND CONDITIONS
**  
**  Redistribution and use in source and binary forms, with or without
**  modification, are permitted provided that the following conditions
**  are met:
**  
**  1 Redistributions of source code must retain the above copyright
**    notice, this list of conditions and the following disclaimer.
**  
**  2 Redistributions in binary form must reproduce the above copyright
**    notice, this list of conditions and the following disclaimer in
**    the documentation and/or other materials provided with the
**    distribution.
**  
**  3 Neither the name of the Standards Of Fundamental Astronomy Board,
**    the International Astronomical Union nor the names of its
**    contributors may be used to endorse or promote products derived
**    from this software without specific prior written permission.
**  
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
**  COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE.
**  
*/
