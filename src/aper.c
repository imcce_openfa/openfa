#include "openfa.h"

void openfaAper(double theta, openfaASTROM *astrom)
/*
**  - - - - - - - -
**   o p e n f a A p e r
**  - - - - - - - -
**
**  In the star-independent astrometry parameters, update only the
**  Earth rotation angle, supplied by the caller explicitly.
**
**  Given:
**     theta   double      Earth rotation angle (radians, Note 2)
**     astrom  openfaASTROM*  star-independent astrometry parameters:
**      pmt    double       not used
**      eb     double[3]    not used
**      eh     double[3]    not used
**      em     double       not used
**      v      double[3]    not used
**      bm1    double       not used
**      bpn    double[3][3] not used
**      along  double       longitude + s' (radians)
**      xpl    double       not used
**      ypl    double       not used
**      sphi   double       not used
**      cphi   double       not used
**      diurab double       not used
**      eral   double       not used
**      refa   double       not used
**      refb   double       not used
**
**  Returned:
**     astrom  openfaASTROM*  star-independent astrometry parameters:
**      pmt    double       unchanged
**      eb     double[3]    unchanged
**      eh     double[3]    unchanged
**      em     double       unchanged
**      v      double[3]    unchanged
**      bm1    double       unchanged
**      bpn    double[3][3] unchanged
**      along  double       unchanged
**      xpl    double       unchanged
**      ypl    double       unchanged
**      sphi   double       unchanged
**      cphi   double       unchanged
**      diurab double       unchanged
**      eral   double       "local" Earth rotation angle (radians)
**      refa   double       unchanged
**      refb   double       unchanged
**
**  Notes:
**
**  1) This function exists to enable sidereal-tracking applications to
**     avoid wasteful recomputation of the bulk of the astrometry
**     parameters:  only the Earth rotation is updated.
**
**  2) For targets expressed as equinox based positions, such as
**     classical geocentric apparent (RA,Dec), the supplied theta can be
**     Greenwich apparent sidereal time rather than Earth rotation
**     angle.
**
**  3) The function openfaAper13 can be used instead of the present
**     function, and starts from UT1 rather than ERA itself.
**
**  4) This is one of several functions that inserts into the astrom
**     structure star-independent parameters needed for the chain of
**     astrometric transformations ICRS <-> GCRS <-> CIRS <-> observed.
**
**     The various functions support different classes of observer and
**     portions of the transformation chain:
**
**          functions         observer        transformation
**
**       openfaApcg openfaApcg13    geocentric      ICRS <-> GCRS
**       openfaApci openfaApci13    terrestrial     ICRS <-> CIRS
**       openfaApco openfaApco13    terrestrial     ICRS <-> observed
**       openfaApcs openfaApcs13    space           ICRS <-> GCRS
**       openfaAper openfaAper13    terrestrial     update Earth rotation
**       openfaApio openfaApio13    terrestrial     CIRS <-> observed
**
**     Those with names ending in "13" use contemporary OPENFA models to
**     compute the various ephemerides.  The others accept ephemerides
**     supplied by the caller.
**
**     The transformation from ICRS to GCRS covers space motion,
**     parallax, light deflection, and aberration.  From GCRS to CIRS
**     comprises frame bias and precession-nutation.  From CIRS to
**     observed takes account of Earth rotation, polar motion, diurnal
**     aberration and parallax (unless subsumed into the ICRS <-> GCRS
**     transformation), and atmospheric refraction.
**
**  Copyright (C) 2018-2024,  IMCCE, CNRS, Observatoire de Paris, PSL.
**  Derived, with permission, from the SOFA library.  See notes at end of file.
# Based on (**
)*/
{
   astrom->eral = theta + astrom->along;

/* Finished. */

}
/*----------------------------------------------------------------------
**  
**  
**  Copyright (C) 2018-2024,  IMCCE, CNRS, Observatoire de Paris, PSL.
**  All rights reserved.
**  
**  This library is derived, with permission, from the International
**  Astronomical Union's "Standards of Fundamental Astronomy" library,
**  available from http://www.iausofa.org
**  and is derived from the ERFA project https://github.com/liberfa/erfa.
**  
**  The OPENFA version is intended to retain identical functionality to
**  the SOFA library, but made distinct through different function and
**  file names, as set out in the SOFA license conditions.  The SOFA
**  original has a role as a reference standard for the IAU and IERS,
**  and consequently redistribution is permitted only in its unaltered
**  state.  The OPENFA version is not subject to this restriction and
**  therefore can be included in distributions which do not support the
**  concept of "read only" software.
**  
**  Although the intent is to replicate the SOFA API (other than
**  replacement of prefix names) and results (with the exception of
**  bugs), SOFA is not
**  responsible for any errors found in this version of the library.
**  
**  If you wish to acknowledge the SOFA heritage, please acknowledge
**  that you are using a library derived from SOFA, rather than SOFA
**  itself.
**  
**  
**  TERMS AND CONDITIONS
**  
**  Redistribution and use in source and binary forms, with or without
**  modification, are permitted provided that the following conditions
**  are met:
**  
**  1 Redistributions of source code must retain the above copyright
**    notice, this list of conditions and the following disclaimer.
**  
**  2 Redistributions in binary form must reproduce the above copyright
**    notice, this list of conditions and the following disclaimer in
**    the documentation and/or other materials provided with the
**    distribution.
**  
**  3 Neither the name of the Standards Of Fundamental Astronomy Board,
**    the International Astronomical Union nor the names of its
**    contributors may be used to endorse or promote products derived
**    from this software without specific prior written permission.
**  
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
**  COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE.
**  
*/
