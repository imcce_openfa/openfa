#ifndef OPENFAMHDEF
#define OPENFAMHDEF

/*
**  - - - - - - - -
**   o p e n f a m . h
**  - - - - - - - -
**
**  Macros used by OPENFA library.
**
**  Copyright (C) 2018-2024,  IMCCE, CNRS, Observatoire de Paris, PSL.
**  Derived, with permission, from the SOFA library.  See notes at end of file.
*/

/* Pi */
#define OpenFA_DPI (3.141592653589793238462643)

/* 2Pi */
#define OpenFA_D2PI (6.283185307179586476925287)

/* Radians to degrees */
#define OpenFA_DR2D (57.29577951308232087679815)

/* Degrees to radians */
#define OpenFA_DD2R (1.745329251994329576923691e-2)

/* Radians to arcseconds */
#define OpenFA_DR2AS (206264.8062470963551564734)

/* Arcseconds to radians */
#define OpenFA_DAS2R (4.848136811095359935899141e-6)

/* Seconds of time to radians */
#define OpenFA_DS2R (7.272205216643039903848712e-5)

/* Arcseconds in a full circle */
#define OpenFA_TURNAS (1296000.0)

/* Milliarcseconds to radians */
#define OpenFA_DMAS2R (OpenFA_DAS2R / 1e3)

/* Length of tropical year B1900 (days) */
#define OpenFA_DTY (365.242198781)

/* Seconds per day. */
#define OpenFA_DAYSEC (86400.0)

/* Days per Julian year */
#define OpenFA_DJY (365.25)

/* Days per Julian century */
#define OpenFA_DJC (36525.0)

/* Days per Julian millennium */
#define OpenFA_DJM (365250.0)

/* Reference epoch (J2000.0), Julian Date */
#define OpenFA_DJ00 (2451545.0)

/* Julian Date of Modified Julian Date zero */
#define OpenFA_DJM0 (2400000.5)

/* Reference epoch (J2000.0), Modified Julian Date */
#define OpenFA_DJM00 (51544.5)

/* 1977 Jan 1.0 as MJD */
#define OpenFA_DJM77 (43144.0)

/* TT minus TAI (s) */
#define OpenFA_TTMTAI (32.184)

/* Astronomical unit (m, IAU 2012) */
#define OpenFA_DAU (149597870.7e3)

/* Speed of light (m/s) */
#define OpenFA_CMPS 299792458.0

/* Light time for 1 au (s) */
#define OpenFA_AULT (OpenFA_DAU/OpenFA_CMPS)

/* Speed of light (au per day) */
#define OpenFA_DC (OpenFA_DAYSEC/OpenFA_AULT)

/* L_G = 1 - d(TT)/d(TCG) */
#define OpenFA_ELG (6.969290134e-10)

/* L_B = 1 - d(TDB)/d(TCB), and TDB (s) at TAI 1977/1/1.0 */
#define OpenFA_ELB (1.550519768e-8)
#define OpenFA_TDB0 (-6.55e-5)

/* Schwarzschild radius of the Sun (au) */
/* = 2 * 1.32712440041e20 / (2.99792458e8)^2 / 1.49597870700e11 */
#define OpenFA_SRS 1.97412574336e-8

/* OpenFA_DINT(A) - truncate to nearest whole number towards zero (double) */
#define OpenFA_DINT(A) ((A)<0.0?ceil(A):floor(A))

/* OpenFA_DNINT(A) - round to nearest whole number (double) */
#define OpenFA_DNINT(A) (fabs(A)<0.5?0.0\
                                :((A)<0.0?ceil((A)-0.5):floor((A)+0.5)))

/* OpenFA_DSIGN(A,B) - magnitude of A with sign of B (double) */
#define OpenFA_DSIGN(A,B) ((B)<0.0?-fabs(A):fabs(A))

/* max(A,B) - larger (most +ve) of two numbers (generic) */
#define OpenFA_GMAX(A,B) (((A)>(B))?(A):(B))

/* min(A,B) - smaller (least +ve) of two numbers (generic) */
#define OpenFA_GMIN(A,B) (((A)<(B))?(A):(B))

/* Reference ellipsoids */
#define OpenFA_WGS84 1
#define OpenFA_GRS80 2
#define OpenFA_WGS72 3

#endif


/*----------------------------------------------------------------------
**  
**  
**  Copyright (C) 2018-2024,  IMCCE, CNRS, Observatoire de Paris, PSL.
**  All rights reserved.
**  
**  This library is derived, with permission, from the International
**  Astronomical Union's "Standards of Fundamental Astronomy" library,
**  available from http://www.iausofa.org
**  and is derived from the ERFA project https://github.com/liberfa/erfa.
**  
**  The OPENFA version is intended to retain identical functionality to
**  the SOFA library, but made distinct through different function and
**  file names, as set out in the SOFA license conditions.  The SOFA
**  original has a role as a reference standard for the IAU and IERS,
**  and consequently redistribution is permitted only in its unaltered
**  state.  The OPENFA version is not subject to this restriction and
**  therefore can be included in distributions which do not support the
**  concept of "read only" software.
**  
**  Although the intent is to replicate the SOFA API (other than
**  replacement of prefix names) and results (with the exception of
**  bugs), SOFA is not
**  responsible for any errors found in this version of the library.
**  
**  If you wish to acknowledge the SOFA heritage, please acknowledge
**  that you are using a library derived from SOFA, rather than SOFA
**  itself.
**  
**  
**  TERMS AND CONDITIONS
**  
**  Redistribution and use in source and binary forms, with or without
**  modification, are permitted provided that the following conditions
**  are met:
**  
**  1 Redistributions of source code must retain the above copyright
**    notice, this list of conditions and the following disclaimer.
**  
**  2 Redistributions in binary form must reproduce the above copyright
**    notice, this list of conditions and the following disclaimer in
**    the documentation and/or other materials provided with the
**    distribution.
**  
**  3 Neither the name of the Standards Of Fundamental Astronomy Board,
**    the International Astronomical Union nor the names of its
**    contributors may be used to endorse or promote products derived
**    from this software without specific prior written permission.
**  
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
**  COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE.
**  
*/
