#ifndef OPENFAHDEF
#define OPENFAHDEF

/*
**  - - - - - - -
**   o p e n f a . h
**  - - - - - - -
**
**  Prototype function declarations for OPENFA library.
**
**  Copyright (C) 2018-2024,  IMCCE, CNRS, Observatoire de Paris, PSL.
**  Derived, with permission, from the SOFA library.  See notes at end of file.
*/

#include "math.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Star-independent astrometry parameters */
typedef struct {
   double pmt;        /* PM time interval (SSB, Julian years) */
   double eb[3];      /* SSB to observer (vector, au) */
   double eh[3];      /* Sun to observer (unit vector) */
   double em;         /* distance from Sun to observer (au) */
   double v[3];       /* barycentric observer velocity (vector, c) */
   double bm1;        /* sqrt(1-|v|^2): reciprocal of Lorenz factor */
   double bpn[3][3];  /* bias-precession-nutation matrix */
   double along;      /* longitude + s' + dERA(DUT) (radians) */
   double phi;        /* geodetic latitude (radians) */
   double xpl;        /* polar motion xp wrt local meridian (radians) */
   double ypl;        /* polar motion yp wrt local meridian (radians) */
   double sphi;       /* sine of geodetic latitude */
   double cphi;       /* cosine of geodetic latitude */
   double diurab;     /* magnitude of diurnal aberration vector */
   double eral;       /* "local" Earth rotation angle (radians) */
   double refa;       /* refraction constant A (radians) */
   double refb;       /* refraction constant B (radians) */
} openfaASTROM;
/* (Vectors eb, eh, em and v are all with respect to BCRS axes.) */

/* Body parameters for light deflection */
typedef struct {
   double bm;         /* mass of the body (solar masses) */
   double dl;         /* deflection limiter (radians^2/2) */
   double pv[2][3];   /* barycentric PV of the body (au, au/day) */
} openfaLDBODY;

/* Astronomy/Calendars */
int openfaCal2jd(int iy, int im, int id, double *djm0, double *djm);
double openfaEpb(double dj1, double dj2);
void openfaEpb2jd(double epb, double *djm0, double *djm);
double openfaEpj(double dj1, double dj2);
void openfaEpj2jd(double epj, double *djm0, double *djm);
int openfaJd2cal(double dj1, double dj2,
                     int *iy, int *im, int *id, double *fd);
int openfaJdcalf(int ndp, double dj1, double dj2, int iymdf[4]);

/* Astronomy/Astrometry */
void openfaAb(double pnat[3], double v[3], double s, double bm1,
           double ppr[3]);
void openfaApcg(double date1, double date2,
             double ebpv[2][3], double ehp[3],
             openfaASTROM *astrom);
void openfaApcg13(double date1, double date2, openfaASTROM *astrom);
void openfaApci(double date1, double date2,
             double ebpv[2][3], double ehp[3],
             double x, double y, double s,
             openfaASTROM *astrom);
void openfaApci13(double date1, double date2,
               openfaASTROM *astrom, double *eo);
void openfaApco(double date1, double date2,
             double ebpv[2][3], double ehp[3],
             double x, double y, double s, double theta,
             double elong, double phi, double hm,
             double xp, double yp, double sp,
             double refa, double refb,
             openfaASTROM *astrom);
int openfaApco13(double utc1, double utc2, double dut1,
              double elong, double phi, double hm, double xp, double yp,
              double phpa, double tc, double rh, double wl,
              openfaASTROM *astrom, double *eo);
void openfaApcs(double date1, double date2, double pv[2][3],
             double ebpv[2][3], double ehp[3],
             openfaASTROM *astrom);
void openfaApcs13(double date1, double date2, double pv[2][3],
               openfaASTROM *astrom);
void openfaAper(double theta, openfaASTROM *astrom);
void openfaAper13(double ut11, double ut12, openfaASTROM *astrom);
void openfaApio(double sp, double theta,
             double elong, double phi, double hm, double xp, double yp,
             double refa, double refb,
             openfaASTROM *astrom);
int openfaApio13(double utc1, double utc2, double dut1,
              double elong, double phi, double hm, double xp, double yp,
              double phpa, double tc, double rh, double wl,
              openfaASTROM *astrom);
void openfaAtcc13(double rc, double dc,
               double pr, double pd, double px, double rv,
               double date1, double date2,
               double *ra, double *da);
void openfaAtccq(double rc, double dc,
              double pr, double pd, double px, double rv,
              openfaASTROM *astrom, double *ra, double *da);
void openfaAtci13(double rc, double dc,
               double pr, double pd, double px, double rv,
               double date1, double date2,
               double *ri, double *di, double *eo);
void openfaAtciq(double rc, double dc, double pr, double pd,
              double px, double rv, openfaASTROM *astrom,
              double *ri, double *di);
void openfaAtciqn(double rc, double dc, double pr, double pd,
               double px, double rv, openfaASTROM *astrom,
               int n, openfaLDBODY b[], double *ri, double *di);
void openfaAtciqz(double rc, double dc, openfaASTROM *astrom,
               double *ri, double *di);
int openfaAtco13(double rc, double dc,
              double pr, double pd, double px, double rv,
              double utc1, double utc2, double dut1,
              double elong, double phi, double hm, double xp, double yp,
              double phpa, double tc, double rh, double wl,
              double *aob, double *zob, double *hob,
              double *dob, double *rob, double *eo);
void openfaAtic13(double ri, double di,
               double date1, double date2,
               double *rc, double *dc, double *eo);
void openfaAticq(double ri, double di, openfaASTROM *astrom,
              double *rc, double *dc);
void openfaAticqn(double ri, double di, openfaASTROM *astrom,
               int n, openfaLDBODY b[], double *rc, double *dc);
int openfaAtio13(double ri, double di,
              double utc1, double utc2, double dut1,
              double elong, double phi, double hm, double xp, double yp,
              double phpa, double tc, double rh, double wl,
              double *aob, double *zob, double *hob,
              double *dob, double *rob);
void openfaAtioq(double ri, double di, openfaASTROM *astrom,
              double *aob, double *zob,
              double *hob, double *dob, double *rob);
int openfaAtoc13(const char *type, double ob1, double ob2,
              double utc1, double utc2, double dut1,
              double elong, double phi, double hm, double xp, double yp,
              double phpa, double tc, double rh, double wl,
              double *rc, double *dc);
int openfaAtoi13(const char *type, double ob1, double ob2,
              double utc1, double utc2, double dut1,
              double elong, double phi, double hm, double xp, double yp,
              double phpa, double tc, double rh, double wl,
              double *ri, double *di);
void openfaAtoiq(const char *type,
              double ob1, double ob2, openfaASTROM *astrom,
              double *ri, double *di);
void openfaLd(double bm, double p[3], double q[3], double e[3],
           double em, double dlim, double p1[3]);
void openfaLdn(int n, openfaLDBODY b[], double ob[3], double sc[3],
            double sn[3]);
void openfaLdsun(double p[3], double e[3], double em, double p1[3]);
void openfaPmpx(double rc, double dc, double pr, double pd,
             double px, double rv, double pmt, double pob[3],
             double pco[3]);
int openfaPmsafe(double ra1, double dec1, double pmr1, double pmd1,
              double px1, double rv1,
              double ep1a, double ep1b, double ep2a, double ep2b,
              double *ra2, double *dec2, double *pmr2, double *pmd2,
              double *px2, double *rv2);
void openfaPvtob(double elong, double phi, double height, double xp,
              double yp, double sp, double theta, double pv[2][3]);
void openfaRefco(double phpa, double tc, double rh, double wl,
              double *refa, double *refb);

/* Astronomy/Ephemerides */
int openfaEpv00(double date1, double date2,
             double pvh[2][3], double pvb[2][3]);
void openfaMoon98(double date1, double date2, double pv[2][3]);
int openfaPlan94(double date1, double date2, int np, double pv[2][3]);

/* Astronomy/FundamentalArgs */
double openfaFad03(double t);
double openfaFae03(double t);
double openfaFaf03(double t);
double openfaFaju03(double t);
double openfaFal03(double t);
double openfaFalp03(double t);
double openfaFama03(double t);
double openfaFame03(double t);
double openfaFane03(double t);
double openfaFaom03(double t);
double openfaFapa03(double t);
double openfaFasa03(double t);
double openfaFaur03(double t);
double openfaFave03(double t);

/* Astronomy/PrecNutPolar */
void openfaBi00(double *dpsibi, double *depsbi, double *dra);
void openfaBp00(double date1, double date2,
             double rb[3][3], double rp[3][3], double rbp[3][3]);
void openfaBp06(double date1, double date2,
             double rb[3][3], double rp[3][3], double rbp[3][3]);
void openfaBpn2xy(double rbpn[3][3], double *x, double *y);
void openfaC2i00a(double date1, double date2, double rc2i[3][3]);
void openfaC2i00b(double date1, double date2, double rc2i[3][3]);
void openfaC2i06a(double date1, double date2, double rc2i[3][3]);
void openfaC2ibpn(double date1, double date2, double rbpn[3][3],
               double rc2i[3][3]);
void openfaC2ixy(double date1, double date2, double x, double y,
              double rc2i[3][3]);
void openfaC2ixys(double x, double y, double s, double rc2i[3][3]);
void openfaC2t00a(double tta, double ttb, double uta, double utb,
               double xp, double yp, double rc2t[3][3]);
void openfaC2t00b(double tta, double ttb, double uta, double utb,
               double xp, double yp, double rc2t[3][3]);
void openfaC2t06a(double tta, double ttb, double uta, double utb,
               double xp, double yp, double rc2t[3][3]);
void openfaC2tcio(double rc2i[3][3], double era, double rpom[3][3],
               double rc2t[3][3]);
void openfaC2teqx(double rbpn[3][3], double gst, double rpom[3][3],
               double rc2t[3][3]);
void openfaC2tpe(double tta, double ttb, double uta, double utb,
              double dpsi, double deps, double xp, double yp,
              double rc2t[3][3]);
void openfaC2txy(double tta, double ttb, double uta, double utb,
              double x, double y, double xp, double yp,
              double rc2t[3][3]);
double openfaEo06a(double date1, double date2);
double openfaEors(double rnpb[3][3], double s);
void openfaFw2m(double gamb, double phib, double psi, double eps,
             double r[3][3]);
void openfaFw2xy(double gamb, double phib, double psi, double eps,
              double *x, double *y);
void openfaLtp(double epj, double rp[3][3]);
void openfaLtpb(double epj, double rpb[3][3]);
void openfaLtpecl(double epj, double vec[3]);
void openfaLtpequ(double epj, double veq[3]);
void openfaNum00a(double date1, double date2, double rmatn[3][3]);
void openfaNum00b(double date1, double date2, double rmatn[3][3]);
void openfaNum06a(double date1, double date2, double rmatn[3][3]);
void openfaNumat(double epsa, double dpsi, double deps, double rmatn[3][3]);
void openfaNut00a(double date1, double date2, double *dpsi, double *deps);
void openfaNut00b(double date1, double date2, double *dpsi, double *deps);
void openfaNut06a(double date1, double date2, double *dpsi, double *deps);
void openfaNut80(double date1, double date2, double *dpsi, double *deps);
void openfaNutm80(double date1, double date2, double rmatn[3][3]);
double openfaObl06(double date1, double date2);
double openfaObl80(double date1, double date2);
void openfaP06e(double date1, double date2,
             double *eps0, double *psia, double *oma, double *bpa,
             double *bqa, double *pia, double *bpia,
             double *epsa, double *chia, double *za, double *zetaa,
             double *thetaa, double *pa,
             double *gam, double *phi, double *psi);
void openfaPb06(double date1, double date2,
             double *bzeta, double *bz, double *btheta);
void openfaPfw06(double date1, double date2,
              double *gamb, double *phib, double *psib, double *epsa);
void openfaPmat00(double date1, double date2, double rbp[3][3]);
void openfaPmat06(double date1, double date2, double rbp[3][3]);
void openfaPmat76(double date1, double date2, double rmatp[3][3]);
void openfaPn00(double date1, double date2, double dpsi, double deps,
             double *epsa,
             double rb[3][3], double rp[3][3], double rbp[3][3],
             double rn[3][3], double rbpn[3][3]);
void openfaPn00a(double date1, double date2,
              double *dpsi, double *deps, double *epsa,
              double rb[3][3], double rp[3][3], double rbp[3][3],
              double rn[3][3], double rbpn[3][3]);
void openfaPn00b(double date1, double date2,
              double *dpsi, double *deps, double *epsa,
              double rb[3][3], double rp[3][3], double rbp[3][3],
              double rn[3][3], double rbpn[3][3]);
void openfaPn06(double date1, double date2, double dpsi, double deps,
             double *epsa,
             double rb[3][3], double rp[3][3], double rbp[3][3],
             double rn[3][3], double rbpn[3][3]);
void openfaPn06a(double date1, double date2,
              double *dpsi, double *deps, double *epsa,
              double rb[3][3], double rp[3][3], double rbp[3][3],
              double rn[3][3], double rbpn[3][3]);
void openfaPnm00a(double date1, double date2, double rbpn[3][3]);
void openfaPnm00b(double date1, double date2, double rbpn[3][3]);
void openfaPnm06a(double date1, double date2, double rnpb[3][3]);
void openfaPnm80(double date1, double date2, double rmatpn[3][3]);
void openfaPom00(double xp, double yp, double sp, double rpom[3][3]);
void openfaPr00(double date1, double date2,
             double *dpsipr, double *depspr);
void openfaPrec76(double date01, double date02,
               double date11, double date12,
               double *zeta, double *z, double *theta);
double openfaS00(double date1, double date2, double x, double y);
double openfaS00a(double date1, double date2);
double openfaS00b(double date1, double date2);
double openfaS06(double date1, double date2, double x, double y);
double openfaS06a(double date1, double date2);
double openfaSp00(double date1, double date2);
void openfaXy06(double date1, double date2, double *x, double *y);
void openfaXys00a(double date1, double date2,
               double *x, double *y, double *s);
void openfaXys00b(double date1, double date2,
               double *x, double *y, double *s);
void openfaXys06a(double date1, double date2,
               double *x, double *y, double *s);

/* Astronomy/RotationAndTime */
double openfaEe00(double date1, double date2, double epsa, double dpsi);
double openfaEe00a(double date1, double date2);
double openfaEe00b(double date1, double date2);
double openfaEe06a(double date1, double date2);
double openfaEect00(double date1, double date2);
double openfaEqeq94(double date1, double date2);
double openfaEra00(double dj1, double dj2);
double openfaGmst00(double uta, double utb, double tta, double ttb);
double openfaGmst06(double uta, double utb, double tta, double ttb);
double openfaGmst82(double dj1, double dj2);
double openfaGst00a(double uta, double utb, double tta, double ttb);
double openfaGst00b(double uta, double utb);
double openfaGst06(double uta, double utb, double tta, double ttb,
                double rnpb[3][3]);
double openfaGst06a(double uta, double utb, double tta, double ttb);
double openfaGst94(double uta, double utb);

/* Astronomy/SpaceMotion */
int openfaPvstar(double pv[2][3], double *ra, double *dec,
              double *pmr, double *pmd, double *px, double *rv);
int openfaStarpv(double ra, double dec,
              double pmr, double pmd, double px, double rv,
              double pv[2][3]);

/* Astronomy/StarCatalogs */
void openfaFk425(double r1950, double d1950,
              double dr1950, double dd1950,
              double p1950, double v1950,
              double *r2000, double *d2000,
              double *dr2000, double *dd2000,
              double *p2000, double *v2000);
void openfaFk45z(double r1950, double d1950, double bepoch,
              double *r2000, double *d2000);
void openfaFk524(double r2000, double d2000,
              double dr2000, double dd2000,
              double p2000, double v2000,
              double *r1950, double *d1950,
              double *dr1950, double *dd1950,
              double *p1950, double *v1950);
void openfaFk52h(double r5, double d5,
              double dr5, double dd5, double px5, double rv5,
              double *rh, double *dh,
              double *drh, double *ddh, double *pxh, double *rvh);
void openfaFk54z(double r2000, double d2000, double bepoch,
              double *r1950, double *d1950,
              double *dr1950, double *dd1950);
void openfaFk5hip(double r5h[3][3], double s5h[3]);
void openfaFk5hz(double r5, double d5, double date1, double date2,
              double *rh, double *dh);
void openfaH2fk5(double rh, double dh,
              double drh, double ddh, double pxh, double rvh,
              double *r5, double *d5,
              double *dr5, double *dd5, double *px5, double *rv5);
void openfaHfk5z(double rh, double dh, double date1, double date2,
              double *r5, double *d5, double *dr5, double *dd5);
int openfaStarpm(double ra1, double dec1,
              double pmr1, double pmd1, double px1, double rv1,
              double ep1a, double ep1b, double ep2a, double ep2b,
              double *ra2, double *dec2,
              double *pmr2, double *pmd2, double *px2, double *rv2);

/* Astronomy/EclipticCoordinates */
void openfaEceq06(double date1, double date2, double dl, double db,
               double *dr, double *dd);
void openfaEcm06(double date1, double date2, double rm[3][3]);
void openfaEqec06(double date1, double date2, double dr, double dd,
               double *dl, double *db);
void openfaLteceq(double epj, double dl, double db, double *dr, double *dd);
void openfaLtecm(double epj, double rm[3][3]);
void openfaLteqec(double epj, double dr, double dd, double *dl, double *db);

/* Astronomy/GalacticCoordinates */
void openfaG2icrs(double dl, double db, double *dr, double *dd);
void openfaIcrs2g(double dr, double dd, double *dl, double *db);

/* Astronomy/GeodeticGeocentric */
int openfaEform(int n, double *a, double *f);
int openfaGc2gd(int n, double xyz[3],
             double *elong, double *phi, double *height);
int openfaGc2gde(double a, double f, double xyz[3],
              double *elong, double *phi, double *height);
int openfaGd2gc(int n, double elong, double phi, double height,
             double xyz[3]);
int openfaGd2gce(double a, double f,
              double elong, double phi, double height, double xyz[3]);

/* Astronomy/Timescales */
int openfaD2dtf(const char *scale, int ndp, double d1, double d2,
             int *iy, int *im, int *id, int ihmsf[4]);
int openfaDat(int iy, int im, int id, double fd, double *deltat);
double openfaDtdb(double date1, double date2,
               double ut, double elong, double u, double v);
int openfaDtf2d(const char *scale, int iy, int im, int id,
             int ihr, int imn, double sec, double *d1, double *d2);
int openfaTaitt(double tai1, double tai2, double *tt1, double *tt2);
int openfaTaiut1(double tai1, double tai2, double dta,
              double *ut11, double *ut12);
int openfaTaiutc(double tai1, double tai2, double *utc1, double *utc2);
int openfaTcbtdb(double tcb1, double tcb2, double *tdb1, double *tdb2);
int openfaTcgtt(double tcg1, double tcg2, double *tt1, double *tt2);
int openfaTdbtcb(double tdb1, double tdb2, double *tcb1, double *tcb2);
int openfaTdbtt(double tdb1, double tdb2, double dtr,
             double *tt1, double *tt2);
int openfaTttai(double tt1, double tt2, double *tai1, double *tai2);
int openfaTttcg(double tt1, double tt2, double *tcg1, double *tcg2);
int openfaTttdb(double tt1, double tt2, double dtr,
             double *tdb1, double *tdb2);
int openfaTtut1(double tt1, double tt2, double dt,
             double *ut11, double *ut12);
int openfaUt1tai(double ut11, double ut12, double dta,
              double *tai1, double *tai2);
int openfaUt1tt(double ut11, double ut12, double dt,
             double *tt1, double *tt2);
int openfaUt1utc(double ut11, double ut12, double dut1,
              double *utc1, double *utc2);
int openfaUtctai(double utc1, double utc2, double *tai1, double *tai2);
int openfaUtcut1(double utc1, double utc2, double dut1,
              double *ut11, double *ut12);

/* Astronomy/HorizonEquatorial */
void openfaAe2hd(double az, double el, double phi,
              double *ha, double *dec);
void openfaHd2ae(double ha, double dec, double phi,
              double *az, double *el);
double openfaHd2pa(double ha, double dec, double phi);

/* Astronomy/Gnomonic */
int openfaTpors(double xi, double eta, double a, double b,
             double *a01, double *b01, double *a02, double *b02);
int openfaTporv(double xi, double eta, double v[3],
             double v01[3], double v02[3]);
void openfaTpsts(double xi, double eta, double a0, double b0,
              double *a, double *b);
void openfaTpstv(double xi, double eta, double v0[3], double v[3]);
int openfaTpxes(double a, double b, double a0, double b0,
             double *xi, double *eta);
int openfaTpxev(double v[3], double v0[3], double *xi, double *eta);

/* VectorMatrix/AngleOps */
void openfaA2af(int ndp, double angle, char *sign, int idmsf[4]);
void openfaA2tf(int ndp, double angle, char *sign, int ihmsf[4]);
int openfaAf2a(char s, int ideg, int iamin, double asec, double *rad);
double openfaAnp(double a);
double openfaAnpm(double a);
void openfaD2tf(int ndp, double days, char *sign, int ihmsf[4]);
int openfaTf2a(char s, int ihour, int imin, double sec, double *rad);
int openfaTf2d(char s, int ihour, int imin, double sec, double *days);

/* VectorMatrix/BuildRotations */
void openfaRx(double phi, double r[3][3]);
void openfaRy(double theta, double r[3][3]);
void openfaRz(double psi, double r[3][3]);

/* VectorMatrix/CopyExtendExtract */
void openfaCp(double p[3], double c[3]);
void openfaCpv(double pv[2][3], double c[2][3]);
void openfaCr(double r[3][3], double c[3][3]);
void openfaP2pv(double p[3], double pv[2][3]);
void openfaPv2p(double pv[2][3], double p[3]);

/* VectorMatrix/Initialization */
void openfaIr(double r[3][3]);
void openfaZp(double p[3]);
void openfaZpv(double pv[2][3]);
void openfaZr(double r[3][3]);

/* VectorMatrix/MatrixOps */
void openfaRxr(double a[3][3], double b[3][3], double atb[3][3]);
void openfaTr(double r[3][3], double rt[3][3]);

/* VectorMatrix/MatrixVectorProducts */
void openfaRxp(double r[3][3], double p[3], double rp[3]);
void openfaRxpv(double r[3][3], double pv[2][3], double rpv[2][3]);
void openfaTrxp(double r[3][3], double p[3], double trp[3]);
void openfaTrxpv(double r[3][3], double pv[2][3], double trpv[2][3]);

/* VectorMatrix/RotationVectors */
void openfaRm2v(double r[3][3], double w[3]);
void openfaRv2m(double w[3], double r[3][3]);

/* VectorMatrix/SeparationAndAngle */
double openfaPap(double a[3], double b[3]);
double openfaPas(double al, double ap, double bl, double bp);
double openfaSepp(double a[3], double b[3]);
double openfaSeps(double al, double ap, double bl, double bp);

/* VectorMatrix/SphericalCartesian */
void openfaC2s(double p[3], double *theta, double *phi);
void openfaP2s(double p[3], double *theta, double *phi, double *r);
void openfaPv2s(double pv[2][3],
             double *theta, double *phi, double *r,
             double *td, double *pd, double *rd);
void openfaS2c(double theta, double phi, double c[3]);
void openfaS2p(double theta, double phi, double r, double p[3]);
void openfaS2pv(double theta, double phi, double r,
             double td, double pd, double rd,
             double pv[2][3]);

/* VectorMatrix/VectorOps */
double openfaPdp(double a[3], double b[3]);
double openfaPm(double p[3]);
void openfaPmp(double a[3], double b[3], double amb[3]);
void openfaPn(double p[3], double *r, double u[3]);
void openfaPpp(double a[3], double b[3], double apb[3]);
void openfaPpsp(double a[3], double s, double b[3], double apsb[3]);
void openfaPvdpv(double a[2][3], double b[2][3], double adb[2]);
void openfaPvm(double pv[2][3], double *r, double *s);
void openfaPvmpv(double a[2][3], double b[2][3], double amb[2][3]);
void openfaPvppv(double a[2][3], double b[2][3], double apb[2][3]);
void openfaPvu(double dt, double pv[2][3], double upv[2][3]);
void openfaPvup(double dt, double pv[2][3], double p[3]);
void openfaPvxpv(double a[2][3], double b[2][3], double axb[2][3]);
void openfaPxp(double a[3], double b[3], double axb[3]);
void openfaS2xpv(double s1, double s2, double pv[2][3], double spv[2][3]);
void openfaSxp(double s, double p[3], double sp[3]);
void openfaSxpv(double s, double pv[2][3], double spv[2][3]);

#ifdef __cplusplus
}
#endif

#endif


/*----------------------------------------------------------------------
**  
**  
**  Copyright (C) 2018-2024,  IMCCE, CNRS, Observatoire de Paris, PSL.
**  All rights reserved.
**  
**  This library is derived, with permission, from the International
**  Astronomical Union's "Standards of Fundamental Astronomy" library,
**  available from http://www.iausofa.org
**  and is derived from the ERFA project https://github.com/liberfa/erfa.
**  
**  The OPENFA version is intended to retain identical functionality to
**  the SOFA library, but made distinct through different function and
**  file names, as set out in the SOFA license conditions.  The SOFA
**  original has a role as a reference standard for the IAU and IERS,
**  and consequently redistribution is permitted only in its unaltered
**  state.  The OPENFA version is not subject to this restriction and
**  therefore can be included in distributions which do not support the
**  concept of "read only" software.
**  
**  Although the intent is to replicate the SOFA API (other than
**  replacement of prefix names) and results (with the exception of
**  bugs), SOFA is not
**  responsible for any errors found in this version of the library.
**  
**  If you wish to acknowledge the SOFA heritage, please acknowledge
**  that you are using a library derived from SOFA, rather than SOFA
**  itself.
**  
**  
**  TERMS AND CONDITIONS
**  
**  Redistribution and use in source and binary forms, with or without
**  modification, are permitted provided that the following conditions
**  are met:
**  
**  1 Redistributions of source code must retain the above copyright
**    notice, this list of conditions and the following disclaimer.
**  
**  2 Redistributions in binary form must reproduce the above copyright
**    notice, this list of conditions and the following disclaimer in
**    the documentation and/or other materials provided with the
**    distribution.
**  
**  3 Neither the name of the Standards Of Fundamental Astronomy Board,
**    the International Astronomical Union nor the names of its
**    contributors may be used to endorse or promote products derived
**    from this software without specific prior written permission.
**  
**  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
**  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
**  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
**  FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
**  COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
**  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
**  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
**  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
**  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
**  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
**  POSSIBILITY OF SUCH DAMAGE.
**  
*/
