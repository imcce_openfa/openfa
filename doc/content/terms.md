+++
title = "Legal notices"
weight = 101
chapter = false
pre = ""
+++


IMCCE - Observatoire de Paris  
77 Avenue Denfert-Rochereau  
F-75014 PARIS  

 - Director of the publication : Philippe Robutel
 - Site housed at : IMCCE

## Copyright ©

The written or iconographic documents presented on this server may not, under any circumstances, be used or reproduced without the explicit authorization of the IMCCE or their authors. The use of nominative information for the purposes of other registered treatments (for example for commercial or advertising purposes) is strictly prohibited.

## Conditions of use

### Clause of non-responsibility

The information and other elements appearing on this site are only indicative. Any part, or the entirety, of the information on this site are given as is, without any guarantee of any sort, whether direct or implied, and without limitation, concerning availability, exactitude or completeness.

The IMCCE cannot be held responsible for any prejudice, direct or indirect, resulting from the use of information on this site, which can contain errors, inexactitudes or omissions, and which can be modified at any moment.

The IMCCE cannot be held responsible for accidental prejudice, direct or indirect, resulting from the use of part of all of the elements on the site, nor for any interruption or any other failure to access the data contained on the site.

### Intellectual Property

The site of the IMCCE is a creative work, the exclusive property of the IMCCE, protected by the French and International legislation relative to intellectual property. No reproduction or manifestation can be done which contravenes the rights of the IMCCE as defined by the above legislation.

This site is the property of the ministère de l’Enseignement supérieur, de la Recherche et de l’Innovation and of the Centre national de la recherche scientifique (CNRS).


### Hypertext links

The installation of hypertext links by third parties to pages or documents which are on the site of the IMCCE, is authorized on condition that the links do not go against the interests of the IMCCE, and that they guarantee that the user can identify the origin and the author of the document. IMCCE declines all responsibility for internet sites with a link to the IMCCE.

The site of the IMCCE can have links to other internet sites. These links are inserted only to facilitate access, and IMCCE declines any responsibility for their content.

