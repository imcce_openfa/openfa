+++
title = "MacOS"
weight = 2
chapter = false
pre = ""
+++


The following steps explain how to add install openfa package using [Homebrew](https://brew.sh/) on MacOS. The following instructions assume that Homebrew is already installed.


## Install the openfa package

The following instruction install the [openfa package](https://formulae.brew.sh/formula/openfa) using Homebrew  

```shell
brew install openfa
```
