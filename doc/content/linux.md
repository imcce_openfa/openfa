+++
title = "Linux"
weight = 1
chapter = false
pre = ""
+++


The following steps explain how to add install openfa package on Linux

First, you must add, initially only, the IMCCE repository corresponding to your distribution. To do this, follow the instructions on [packages.imcce.fr](https://packages.imcce.fr).


{{< tabs title="Distributions" >}}
{{% tab title="Debian/Ubuntu" %}}
The second step installs the development package and runtime package of openfa.

```shell
sudo apt install libopenfa2 libopenfa-dev
```

{{% /tab %}}

{{% tab title="Fedora/RHEL/Rocky" %}}
The second step installs the development package and runtime package of openfa.

```shell
sudo dnf install libopenfa2 openfa-devel
```
{{% /tab %}}
{{% tab title="Opensuse" %}}

```shell
sudo zypper install libopenfa2 openfa-devel
```

{{% /tab %}}
{{% tab title="Arch" %}}
The second step installs the development package and runtime package of openfa.

```shell
sudo pacman -Sy openfa 
```

{{% /tab %}}
{{< /tabs >}}

