+++
title = "Windows"
weight = 3
chapter = false
pre = ""
+++

The following steps explain how to add install openfa package on Windows.


{{< tabs title="Distributions" >}}
{{% tab title="vcpkg" %}}
The following step install openfa using [vckg](https://vcpkg.io/en/) on Windows. The following instructions assume that vckg is already installed.
```shell
vcpkg install imcce-openfa
```
{{% /tab %}}
{{% tab title="MSYS2" %}}
The following step install openfa using [MSYS2](https://www.msys2.org/) on Windows. The following instructions assume that msys2 is already installed.

Different packages exist for different architecture and compilers : [mingw-w64-openfa](https://packages.msys2.org/base/mingw-w64-openfa). So you may replace the name of the package by the adapted package's name.
```shell
pacman -Sy mingw-w64-x86_64-openfa
```
{{% /tab %}}
{{< /tabs >}}

